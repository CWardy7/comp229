package oldWorks;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class Task3 extends JPanel {	
	
	public void paint(Graphics g){
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.RED);
		g2.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		g2.setColor(Color.BLACK);
		
		int minX = 10;
		int minY = 10;
		int maxX = this.getWidth();
		int maxY = this.getHeight();
		
		int currentX = minX;
		int currentY = minY;
		
		while (currentX < maxX){
			g2.drawLine(currentX, currentY, currentX, maxY-10);
			
			currentX += 35;
		}
		
		currentX = minX;
		
		while (currentY < maxY){
			g2.drawLine(currentX, currentY, maxX-10, currentY);
		}
			
	}
	

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setSize(1280, 720);
		
		Task3 p = new Task3();
		frame.add(p);
		
		frame.setVisible(true);
		
		
	}
}
