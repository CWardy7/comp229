package myWork;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

public class Cell extends Rectangle{
	//constructors
	
	public Cell(int x, int y, int width, int height){
		super(x, y, width, height);
	}
	
	public void paint(Graphics g, Point mousePosition){
		Graphics2D g2 = (Graphics2D) g;
		
/*??*/		Color c = mousePosition != null && this.contains(mousePosition) ? Color.ORANGE : Color.LIGHT_GRAY;
		g2.setColor(c);
		g2.fill(this);
		
		g2.setColor(Color.BLACK);
		g2.draw(this);
	}
}

	