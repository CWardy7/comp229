package myWork;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Canvas;


public class Main {
	
	private Frame window;
	private Canvas canvas;
	private Grid grid;

	public Main(){
		window = new Frame();
		window.setSize(1280, 720);
		
		canvas = new Canvas();
		canvas.setPreferredSize(new Dimension(1280, 720));
		window.add(canvas);
		
		grid = new Grid();
		grid.setBounds(50, 50, grid.getWidth(), grid.getHeight());
		window.add(grid);
		
		window.pack();
		window.setVisible(true);
	}
	
	public void run() {		
		long frameTime = 1000000000 / 60;
		long lastFrame = System.nanoTime();
		while (true) {
			long now = System.nanoTime();
			if(now - lastFrame > frameTime) { 
				grid.repaint();
				lastFrame = now;
			}
		}
	}
		
		
	
	public static void main(String[] args) {
		Main m = new Main();
		m.run();
	}
}