package myWork;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Canvas;

@SuppressWarnings("serial")
public class Grid extends Canvas{

	public static final int NUMBEROFCELLS = 20;
	public static final int CELLSIZE = 35;
	public static final int CANVASSIZE = NUMBEROFCELLS * CELLSIZE + 1;

	public Cell[][]cells= new Cell[NUMBEROFCELLS][NUMBEROFCELLS];{

		for (int i = 0; i < NUMBEROFCELLS; i++){
			for (int k = 0; k < NUMBEROFCELLS; k++){

				cells[i][k] = new Cell(i * CELLSIZE, k * CELLSIZE, CELLSIZE, CELLSIZE);
			}
		}
	}

	public void paint(Graphics g){
		Point mousePosition = getMousePosition();
		
		for(int i = 0; i < NUMBEROFCELLS; i++){
			for(int k = 0; k < NUMBEROFCELLS; k++){
				cells[i][k].paint(g,  mousePosition);
			}
		}

	}

}
